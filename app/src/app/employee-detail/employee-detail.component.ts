import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { WeeklySheetService } from '../services/weeklysheet.service';
import { TaskService } from '../services/task.service';

import { Task } from '../models/task';
import { Employee } from '../models/employee';
import { ActivatedRoute } from '@angular/router';
import { Weeklysheet } from '../models/weeklysheet';

import { Weeks } from '../models/weeks';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss']
})
export class EmployeeDetailComponent implements OnInit {
  employees: Employee[];
  selectedEmployee: Employee;

  tasks: Task[];
  employeeData:  Weeklysheet[];
  newSheetItem: Weeklysheet;

  weeks: number[] = Weeks;
  weekNumber: number;

  id: any;
  isVisible: boolean;

  constructor(private employeeService: EmployeeService,
              private weeklySheetService: WeeklySheetService,
              private taskService:  TaskService,
              private route: ActivatedRoute) {
    this.route.params.subscribe(params => this.id = params.id);
  }

  ngOnInit() {
    this.weekNumber = 1;
    this.isVisible = false;
    this.getAllEmployees();
    this.getAllTasks();
  }

  getAllEmployees(): void {
    this.employeeService.getallemployees().subscribe(data => {
        this.employees = data;

        this.selectedEmployee = data.find(x => x.id == this.id);

        this.getEmployeeSheetWeek(this.selectedEmployee.id, this.weekNumber);
    }, error => {
        console.log(error);
    });
  }

  onSelect(data: number) {
    this.getEmployeeSheetWeek(this.selectedEmployee.id, this.weekNumber);
  }

  onSelect2(employee: number): void {
    this.getEmployeeSheetWeek(employee, this.weekNumber);
  }

  getEmployeeSheetWeek(id: number, week: number): void {
    console.log('id: ' + id);
    console.log('week: ' + week);
    this.weeklySheetService.getallWeeklySheetsPerEmployeePerWeek(id, week).subscribe(data => {
      this.employeeData = [];
      this.employeeData = data;

      this.selectedEmployee = this.employees.find(x => x.id == id);
    }, error => {
      console.log(error);
    });
  }

  // getEmployeeSheet(id: number): void {
  //   // console.log('id: ' + id);
  //   this.weeklySheetService.getallWeeklySheetsPerEmployee(id).subscribe(data => {
  //     this.employeeData = data;

  //     this.selectedEmployee = this.employees.find(x => x.id == id);
  //   }, error => {
  //     console.log(error);
  //   });
  // }

  getAllTasks(): void {
    this.taskService.getAllTasks().subscribe(data => {
      this.tasks = data;
      // console.log(data);
    }, error => {
      console.log(error);
    });
  }

  addTask(): void {
    const newData: Weeklysheet = {
      id: 0,
      employeeId: this.selectedEmployee.id,
      monday: 0,
      friday: 0,
      task: 1,
      sunday: 0,
      saturday: 0,
      tuesday: 0,
      thursday: 0,
      wednesday: 0,
      weekId: 0
    };

    this.employeeData.push(newData);
  }

  saveTimeSheet(): void {
    console.log(this.weekNumber);
    console.log(this.employeeData);

    for (const entry of this.employeeData) {
      entry.weekId = this.weekNumber;
    }

    const jd = JSON.stringify(this.employeeData);
    const jsonData = '{\"sheets\": ' + jd + '}';
    this.weeklySheetService.addWeeklySheet(jsonData).subscribe(data => {
      console.log('success');
      this.isVisible = true;
    }, error => {
      console.log(error);
    });
  }
}
