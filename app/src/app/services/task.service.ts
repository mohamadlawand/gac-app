import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Task } from '../models/task';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class TaskService {
  private baseapi = environment.apiUrl;
  constructor(private http: HttpClient) { }

  /** GET tasks from the server */
  getAllTasks (): Observable<Task[]> {
    return this.http.get<Task[]>(this.baseapi + '/task/getall');
  }
}
