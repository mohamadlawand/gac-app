import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Weeklysheet } from '../models/weeklysheet';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class WeeklySheetService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    /** GET sheets from the server */
    getAllWeeklySheets (): Observable<Weeklysheet[]> {
      return this.http.get<Weeklysheet[]>(this.baseapi + '/WeeklySheet/getall');
    }

    /** GET sheets from the server */
    getallWeeklySheetsPerEmployee (id: number): Observable<Weeklysheet[]> {
      return this.http.get<Weeklysheet[]>(this.baseapi + '/WeeklySheet/getallperemployee/' + id);
    }

    /** GET sheets from the server */
    getallWeeklySheetsPerEmployeePerWeek (id: number, week: number): Observable<Weeklysheet[]> {
      return this.http.get<Weeklysheet[]>(this.baseapi + '/WeeklySheet/GetAllPerEmployeeWeek/' + id + '?weekid=' + week);
    }

    //////// Save methods //////////

    /** POST: add a new hero to the server */
    addWeeklySheet (sheet: any): Observable<Weeklysheet> {
      console.log(sheet);
      return this.http.post<Weeklysheet>(this.baseapi + '/weeklysheet/AddWeeklysheetBatch', sheet, httpOptions);
    }
}
