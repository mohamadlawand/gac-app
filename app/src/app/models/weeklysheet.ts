export class Weeklysheet {
    id: number;
    sunday: number;
    monday: number;
    tuesday: number;
    wednesday: number;
    thursday: number;
    friday: number;
    saturday: number;
    task: number;
    employeeId: number;
    weekId: number;
}
